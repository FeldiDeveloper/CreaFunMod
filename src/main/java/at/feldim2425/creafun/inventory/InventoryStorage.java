package at.feldim2425.creafun.inventory;

import java.util.Arrays;

import at.feldim2425.creafun.utils.NBTTypes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.text.ITextComponent;

public class InventoryStorage implements IInventory{
	
	private ItemStack slots[];
	 
	public InventoryStorage(int size) {
		this.slots = new ItemStack[size];
		
		for(int i = 0; i<size; i++) {
			this.slots[i] = ItemStack.EMPTY;
		}
	}
	
	
	@Override
	public String getName() {
		return null;
	}

	@Override
	public boolean hasCustomName() {
		return false;
	}

	@Override
	public ITextComponent getDisplayName() {
		return null;
	}

	@Override
	public int getSizeInventory() {
		return slots.length;
	}

	@Override
	public boolean isEmpty() {
		return !Arrays.asList(this.slots).stream().filter((stack) -> !stack.isEmpty()).findAny().isPresent();
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		if(index < slots.length) {
			return slots[index];
		}
		return null;
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		if (index >= slots.length || count < 1) {
			return ItemStack.EMPTY;
		}
		ItemStack stack = slots[index];
		if (stack == null || stack.isEmpty()) {
			return ItemStack.EMPTY;
		}
		ItemStack ret = stack.splitStack(count);
		if (stack.getCount() < 1) {
			slots[index] = ItemStack.EMPTY;
		}
		return ret;
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		if (index >= slots.length) {
			return ItemStack.EMPTY;
		}
		ItemStack stack = slots[index];
		slots[index] = ItemStack.EMPTY;
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if (index < slots.length) {
			slots[index] = stack;
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public void markDirty() {
	}

	@Override
	public boolean isUsableByPlayer(EntityPlayer player) {
		return true;
	}

	@Override
	public void openInventory(EntityPlayer player) {
	}

	@Override
	public void closeInventory(EntityPlayer player) {
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		return true;
	}

	@Override
	public int getField(int id) {
		return 0;
	}

	@Override
	public void setField(int id, int value) {
	}

	@Override
	public int getFieldCount() {
		return 0;
	}

	@Override
	public void clear() {
		for(int i = 0; i < slots.length; i++) {
			slots[i] = ItemStack.EMPTY;
		}
	}
	
	public void writeToNBT(NBTTagCompound nbt) {
		NBTTagList nbtList = new NBTTagList();
		for (int i = 0; i < slots.length; i++) {
			ItemStack stack = this.slots[i];
			if (stack.isEmpty()) {
				continue;
			}
			NBTTagCompound slotNBT = new NBTTagCompound();
			NBTTagCompound itemNBT = new NBTTagCompound();
			stack.writeToNBT(itemNBT);
			slotNBT.setInteger("slot", i);
			slotNBT.setTag("item", itemNBT);
			nbtList.appendTag(slotNBT);
		}
		nbt.setTag("slots", nbtList);
		nbt.setInteger("size", this.slots.length);
	}

	public static InventoryStorage readFromNBT(NBTTagCompound nbt) {
		if (nbt.hasKey("size", NBTTypes.INT.getTypeID())) {
			InventoryStorage inventory = new InventoryStorage(nbt.getInteger("size"));
			
			if (nbt.hasKey("slots", NBTTypes.TAG_LIST.getTypeID())) {
				NBTTagList nbtList = (NBTTagList) nbt.getTag("slots");
				for (int i = 0; i < nbtList.tagCount(); i++) {
					NBTTagCompound slotNBT = nbtList.getCompoundTagAt(i);
					if (!slotNBT.hasKey("item", NBTTypes.TAG_COMPOUND.getTypeID()) || !slotNBT.hasKey("slot", NBTTypes.INT.getTypeID())) {
						continue;
					}
					ItemStack stack = new ItemStack(slotNBT.getCompoundTag("item"));
					int slot = slotNBT.getInteger("slot");
					if (slot < inventory.slots.length) {
						inventory.slots[slot] = stack;
					}
				}
			}
			
			return inventory;
		}
		
		return null;
	}
}
