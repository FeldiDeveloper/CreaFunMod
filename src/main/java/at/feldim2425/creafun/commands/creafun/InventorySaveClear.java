package at.feldim2425.creafun.commands.creafun;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import at.feldim2425.creafun.server.capabilities.IPlayerInventoryRepository;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.translation.I18n;

@SuppressWarnings("deprecation")
public class InventorySaveClear extends CommandBase{

	@Override
	public String getName() {
		return "invSavClear";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "commands.creafun.invSavClear.usage";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length != 2 || args[1].isEmpty())
		{
			throw new WrongUsageException(getUsage(sender));
		}
		
		int index = 0;
		
		try {
			index = Integer.parseInt(args[1]);
		}
		catch(NumberFormatException e) {
			throw new WrongUsageException(getUsage(sender));
		}
		
		if(index > 8 || index < 0) {
			throw new CommandException(I18n.translateToLocal("commands.creafun.invSwap.range"));
		}
		
		for(EntityPlayerMP player : getPlayers(server, sender, args[0])) {
			IPlayerInventoryRepository save = player.getCapability(IPlayerInventoryRepository.CAPABILITY, null);
			save.deleteStoredInv(index);
			
			if(sender.sendCommandFeedback()) {
				sender.sendMessage(new TextComponentString(String.format(I18n.translateToLocal("commands.creafun.invSavClear.success"), player.getName(), index)));
			}
		}
	}
	
	@Override
	public int getRequiredPermissionLevel() 
	{
		return 2;
	}
	
	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos)
	{
	    return args.length == 1 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : Collections.emptyList();
	}

}
