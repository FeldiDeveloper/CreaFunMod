package at.feldim2425.creafun.commands.creafun;

import net.minecraft.command.ICommandSender;
import net.minecraftforge.server.command.CommandTreeBase;


public class CreaFunCMD extends CommandTreeBase {
	
	public CreaFunCMD() {
		super();
		
		addSubcommand(new EmcGive());
		addSubcommand(new FullKnowledge());
		addSubcommand(new InventorySaveClear());
		addSubcommand(new InventorySwap());
		addSubcommand(new InventorySave());
		addSubcommand(new InventoryLoad());
	}

	@Override
	public String getName() {
		return "creafun";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "commands.creafun.main.usage";
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 0;
	}
	
	
	


}
