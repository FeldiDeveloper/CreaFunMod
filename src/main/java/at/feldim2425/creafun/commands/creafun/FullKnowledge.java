package at.feldim2425.creafun.commands.creafun;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import moze_intel.projecte.api.ProjectEAPI;
import moze_intel.projecte.api.capabilities.IKnowledgeProvider;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.translation.I18n;

@SuppressWarnings("deprecation")
public class FullKnowledge extends CommandBase{

	@Override
	public String getName() {
		return "fullKnowledge";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "commands.creafun.fullKnowledge.usage";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length != 1)
		{
			throw new WrongUsageException(getUsage(sender));
		}
		
		for(EntityPlayerMP player : getPlayers(server, sender, args[0])) {
			IKnowledgeProvider provider = player.getCapability(ProjectEAPI.KNOWLEDGE_CAPABILITY, null);
			provider.setFullKnowledge(true);
			provider.sync(player);
			
			if(sender.sendCommandFeedback()) {
				sender.sendMessage(new TextComponentString(String.format(I18n.translateToLocal("commands.creafun.fullKnowledge.success"), player.getName())));
			}
		}
	}
	
	@Override
	public int getRequiredPermissionLevel() 
	{
		return 2;
	}
	
	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos)
	{
	    return args.length == 1 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : Collections.emptyList();
	}

}
