package at.feldim2425.creafun;

import java.io.File;

import net.minecraftforge.common.config.Configuration;

public class Config {
	
	private static Configuration config;
	
	private static final String CAT_AUTOSHUTDOWN = "autoshutdown";
	private static final String CAT_MESSAGES = "messages";
	
	public static boolean shutdown_enabled;
	public static int shutdown_uptime;
	public static boolean shutdown_warning;
	
	public static String message_shutdown;
	public static String message_shutdownKick;
	
	public static void init(File configFile) {
		config = new Configuration(configFile);
		
		config.setCategoryComment(CAT_AUTOSHUTDOWN, "Settings for automatic server shutdown");
		
		shutdown_enabled = config.get(CAT_AUTOSHUTDOWN, "enabled", false, "If autoshutdown is enabled. Default: false (no autoshutdown)").getBoolean();
		shutdown_uptime = config.get(CAT_AUTOSHUTDOWN, "uptime", 60 * 6, "Time in minutes to the next shutdown. By default every 6 hours").getInt();
		shutdown_warning = config.get(CAT_AUTOSHUTDOWN, "warning", true, "If true the server will start warning all online players 30, 15, 10, 5, 3 and 1 minute before shutdown.").getBoolean();

	
		config.setCategoryComment(CAT_MESSAGES, "Messages sent to players. (Only for non-moderator features, so you can't change Admin Command messages here)");
		
		message_shutdown = config.get(CAT_MESSAGES, "shutdown", "§cWARNING: <> minutes until shutdown!", "Message in chat that to warn players. <> for the minutes, Color codes work in this message").getString();
		message_shutdownKick = config.get(CAT_MESSAGES, "shutdownKick", "Server restarting!", "Reason for the player kick. Will show up on the clients when the server shuts down.").getString();
		
		config.save();
	}
	
	
	
	private Config() {}

}
