package at.feldim2425.creafun.common;

import at.feldim2425.creafun.server.EventHandlerServer;
import at.feldim2425.creafun.server.capabilities.IPlayerInventoryRepository;
import net.minecraftforge.common.capabilities.CapabilityManager;

public class CommonProxy {

	public void preInit() {
		CapabilityManager.INSTANCE.register(IPlayerInventoryRepository.class, new IPlayerInventoryRepository.Storage(), new IPlayerInventoryRepository.Factory());
		
		EventHandlerServer.init();
	}

	public void init() {
		// TODO Auto-generated method stub
		
	}

	public void postInit() {
		// TODO Auto-generated method stub
		
	}

}
