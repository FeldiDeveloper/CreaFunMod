package at.feldim2425.creafun.server.capabilities;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import at.feldim2425.creafun.CreaFun;
import at.feldim2425.creafun.inventory.InventoryStorage;
import at.feldim2425.creafun.utils.NBTTypes;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;

public interface IPlayerInventoryRepository {
	
	public final static ResourceLocation KEY_ATTACHMENT = new ResourceLocation(CreaFun.MOD_ID, "PlayerInventoryRepository");
	@CapabilityInject(IPlayerInventoryRepository.class)
	public static Capability<IPlayerInventoryRepository> CAPABILITY = null;
	
	
	void swapInventory(InventoryPlayer player, int repoSlot);
	
	void deleteStoredInv(int repoSlot);
	
	boolean isSlotEmpty(int repoSlot);
	
	void savePlayerInventory(InventoryPlayer player, int repoSlot);
	
	void loadPlayerInventory(InventoryPlayer player, int repoSlot);
	
	InventoryStorage getStoredInventory(int repoSlot);
	

	public static class Storage implements Capability.IStorage<IPlayerInventoryRepository> {
    	
		@Override
		public NBTTagCompound writeNBT(Capability<IPlayerInventoryRepository> capability, IPlayerInventoryRepository instance, EnumFacing side) {
			NBTTagList nbtList = new NBTTagList();
			for(Map.Entry<Integer, InventoryStorage> entry : ((Implementation) instance).repo.entrySet()) {
				NBTTagCompound slotNBT = new NBTTagCompound();
				NBTTagCompound invNBT = new NBTTagCompound();
				entry.getValue().writeToNBT(invNBT);
				slotNBT.setTag("inventory",invNBT);
				slotNBT.setInteger("slot", entry.getKey());
				nbtList.appendTag(slotNBT);
			}
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setTag("inventories", nbtList);
			return nbt;
		}
		
		@Override
		public void readNBT(Capability<IPlayerInventoryRepository> capability, IPlayerInventoryRepository instance, EnumFacing side, NBTBase nbt) {
			if(!(nbt instanceof NBTTagCompound) || !(instance instanceof Implementation)) {
				return;
			}
			
			if (((NBTTagCompound) nbt).hasKey("inventories", NBTTypes.TAG_LIST.getTypeID())) {
				NBTTagList nbtList = (NBTTagList) ((NBTTagCompound) nbt).getTag("inventories");
				for (int i = 0; i < nbtList.tagCount(); i++) {
					NBTTagCompound slotNBT = nbtList.getCompoundTagAt(i);
					if (!slotNBT.hasKey("inventory", NBTTypes.TAG_COMPOUND.getTypeID()) || !slotNBT.hasKey("slot", NBTTypes.INT.getTypeID())) {
						continue;
					}
					InventoryStorage inv = InventoryStorage.readFromNBT(slotNBT.getCompoundTag("inventory"));
					int slot = slotNBT.getInteger("slot");
					((Implementation) instance).repo.put(slot, inv);
				}
			}
		}
	}
	
	public static class Factory implements Callable<IPlayerInventoryRepository> {
		@Override
		public IPlayerInventoryRepository call() throws Exception {
			return new Implementation();
		}
	}
	
	public static class Implementation implements IPlayerInventoryRepository {
		
		private Map<Integer, InventoryStorage> repo = new HashMap<>();
		
		@Override
		public void swapInventory(InventoryPlayer player, int repoSlot) {
			if(repoSlot < 0) {
				return;
			}
			
			int size = player.getSizeInventory();
			InventoryStorage storageFrom = repo.get(repoSlot);
			if (storageFrom == null) {
				storageFrom = new InventoryStorage(player.getSizeInventory());
			}
			InventoryStorage storageTo = storageFrom;
			if(storageTo.getSizeInventory() != player.getSizeInventory()) {
				storageTo = new InventoryStorage(player.getSizeInventory());
			}
			
			for(int i=0; i<size; i++) {
				ItemStack playerSt = player.removeStackFromSlot(i);
				player.setInventorySlotContents(i, storageFrom.getStackInSlot(i));
				storageTo.setInventorySlotContents(i, playerSt);
			}
			repo.put(repoSlot, storageTo);
		}

		@Override
		public void deleteStoredInv(int repoSlot) {
			repo.remove(repoSlot);
		}
		
		@Override
		public void savePlayerInventory(InventoryPlayer player, int repoSlot) {
			if(repoSlot < 0) {
				return;
			}
			
			int size = player.getSizeInventory();
			InventoryStorage storage = repo.get(repoSlot);
			if (storage == null) {
				storage = new InventoryStorage(player.getSizeInventory());
			}
			
			for(int i=0; i<size; i++) {
				ItemStack playerSt = player.getStackInSlot(i);
				storage.setInventorySlotContents(i, playerSt.copy());
			}
			repo.put(repoSlot, storage);
		}
		
		@Override
		public void loadPlayerInventory(InventoryPlayer player, int repoSlot) {
			if(repoSlot < 0) {
				return;
			}
			
			int size = player.getSizeInventory();
			InventoryStorage storage = repo.get(repoSlot);
			if (storage == null) {
				return;
			}
			
			for(int i=0; i<size; i++) {
				player.setInventorySlotContents(i, storage.getStackInSlot(i).copy());
			}
		}
		
		@Override
		public InventoryStorage getStoredInventory(int repoSlot) {
			if(repoSlot < 0) {
				return null;
			}
			return repo.get(repoSlot);
		}

		@Override
		public boolean isSlotEmpty(int repoSlot) {
			return repoSlot < 0 || repo.get(repoSlot) == null;
		}

	}
}
