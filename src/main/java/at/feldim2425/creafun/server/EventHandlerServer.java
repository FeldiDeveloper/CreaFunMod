package at.feldim2425.creafun.server;

import at.feldim2425.creafun.server.autoshutdown.AutoshutdownHandler;
import at.feldim2425.creafun.server.capabilities.IPlayerInventoryRepository;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

public class EventHandlerServer {
	
	public static void init() {
		MinecraftForge.EVENT_BUS.register(new EventHandlerServer());
	}
	
	@SubscribeEvent
	public void onPlayerCapabilityAttach(AttachCapabilitiesEvent<Entity> event) {
		if(!(event.getObject() instanceof EntityPlayerMP)) {
			return;
		}
		
		event.addCapability(IPlayerInventoryRepository.KEY_ATTACHMENT, new ICapabilitySerializable<NBTBase>() {
			
			private IPlayerInventoryRepository cap = IPlayerInventoryRepository.CAPABILITY.getDefaultInstance();
			
			@Override
			public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
				return IPlayerInventoryRepository.CAPABILITY.equals(capability);
			}
			
			@SuppressWarnings("unchecked")
			@Override
			public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
				if(IPlayerInventoryRepository.CAPABILITY.equals(capability)) {
					return (T) cap;
				}
				return null;
			}

			@Override
			public NBTBase serializeNBT() {
				return IPlayerInventoryRepository.CAPABILITY.getStorage().writeNBT(IPlayerInventoryRepository.CAPABILITY, cap, null);
			}

			@Override
			public void deserializeNBT(NBTBase nbt) {
				IPlayerInventoryRepository.CAPABILITY.getStorage().readNBT(IPlayerInventoryRepository.CAPABILITY, cap, null, nbt);
			}
		});
	}
	
	@SubscribeEvent
	public void onPlayerClone(PlayerEvent.Clone event) {
		if(event.isWasDeath()) {
			if(event.getOriginal().hasCapability(IPlayerInventoryRepository.CAPABILITY, null)) {
				IPlayerInventoryRepository repo = event.getOriginal().getCapability(IPlayerInventoryRepository.CAPABILITY, null);
				NBTBase nbt = IPlayerInventoryRepository.CAPABILITY.getStorage().writeNBT(IPlayerInventoryRepository.CAPABILITY, repo, null);
				
				IPlayerInventoryRepository repoNew = event.getEntityPlayer().getCapability(IPlayerInventoryRepository.CAPABILITY, null);
				IPlayerInventoryRepository.CAPABILITY.getStorage().readNBT(IPlayerInventoryRepository.CAPABILITY, repoNew, null, nbt);
			}
		}
	}
	
	
	@SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onServerTick(TickEvent.ServerTickEvent event)
    {
		if(event.phase == Phase.START) {
			AutoshutdownHandler.tick();
		}
    }
}
