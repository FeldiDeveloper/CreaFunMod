package at.feldim2425.creafun.server.autoshutdown;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.Level;

import at.feldim2425.creafun.Config;
import at.feldim2425.creafun.CreaFun;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class AutoshutdownHandler {
	
	private static int[] WARNINGS = new int[] {30, 15, 10, 5, 3, 1};
	private static int timerRuns = 0;
	private static int stopTimerCount = -1;
	private static int warnings = 0;
	private static Timer timer;
	private static MinecraftServer server;
	
	
	public static void init() {
		if(Config.shutdown_enabled) {
			if(FMLCommonHandler.instance().getSide().isClient()) {
				CreaFun.getLogger().log(Level.WARN, "You have enabled AutoShutdown on your Client!!!");
				CreaFun.getLogger().log(Level.WARN, "Please consider disabling that feature in your config by navigating to your config directory -> creafun.cfg -> shutdown -> enabled and set it to false!");
			}
			
			server = FMLCommonHandler.instance().getMinecraftServerInstance();
			
			if(server == null) {
				CreaFun.getLogger().log(Level.WARN, "No server instance found! Can't start shutdown timer.");
				return;
			}
			
			long initialDelay = Config.shutdown_uptime * 60 * 1000;
			
			if(Config.shutdown_warning) {
				warnings = -1;
				for(int i = 0; i < WARNINGS.length; i++) {
					long shorted = initialDelay - WARNINGS[i] * 60 * 1000;
					if(shorted >= 0) {
						initialDelay = shorted;
						warnings = i;
					}
				}
			}
			
			timer = new Timer("CreaFun shutdown timer");
			timer.schedule(new ShutdownTimer(), initialDelay, 60 * 1000);
		}
	}
	
	public static void tick() {
		if(timerRuns < 1) {
			return;
		}
		
		if(Config.shutdown_warning && warnings < WARNINGS.length && warnings >= 0) {
			if(WARNINGS[warnings] <= timerRuns) {
				server.getPlayerList().sendMessage(new TextComponentString(Config.message_shutdown.replace("<>", String.valueOf(WARNINGS[warnings]))));
				warnings++;
			}
			
			if(warnings >= WARNINGS.length) {
				stopTimerCount = timerRuns + 1;
			}
		}
		else if(timerRuns >= stopTimerCount) 
		{
			for ( EntityPlayerMP player : server.getPlayerList().getPlayers() )
	        {
	            player.connection.disconnect(new TextComponentString(Config.message_shutdownKick));
	        }
			server.sendMessage(new TextComponentString(Config.message_shutdownKick));
			server.initiateShutdown();
		}
	}
	
	private static class ShutdownTimer extends TimerTask {
		@Override
		public void run() {
			timerRuns ++;
		}
	}
}
