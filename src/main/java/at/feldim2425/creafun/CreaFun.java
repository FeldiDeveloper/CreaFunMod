package at.feldim2425.creafun;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import at.feldim2425.creafun.commands.creafun.CreaFunCMD;
import at.feldim2425.creafun.common.CommonProxy;
import at.feldim2425.creafun.server.autoshutdown.AutoshutdownHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;


@Mod(modid = CreaFun.MOD_ID, name = CreaFun.NAME, version = CreaFun.VERSION, dependencies = CreaFun.DEPENDENCIES, acceptableRemoteVersions="*")
public class CreaFun {

	public static final String	MOD_ID			= "creafun";
	public static final String	NAME			= "CreaFun";
	public static final String	VERSION			= "0.1.4";
	public static final String	DEPENDENCIES	= "required-after:forge@[14.23.2.2644,);"
			+ "required-after:projecte@[1.12-PE1.3.0,);";

	@Instance
	private static CreaFun		instance;
	@SidedProxy(serverSide = "at.feldim2425.creafun.common.CommonProxy", clientSide = "at.feldim2425.creafun.common.CommonProxy")
	private static CommonProxy	proxy;

	private static Logger			logger	= LogManager.getLogger(CreaFun.NAME);

	public static CreaFun getInstance() {
		return instance;
	}

	public static Logger getLogger() {
		return logger;
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		Config.init(event.getSuggestedConfigurationFile());
		
		proxy.preInit();
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		proxy.init();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		proxy.postInit();
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent event)
	{
		event.registerServerCommand(new CreaFunCMD());
	}
	
	@EventHandler
	public void serverStarted(FMLServerStartedEvent event)
	{
		AutoshutdownHandler.init();
	}
}
